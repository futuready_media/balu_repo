<div class="fixed-top-head">
      <div class="container content-for-fixed-head">
        
      </div>
    </div>
    <!-- <div class="clearfix"></div> -->
     <!-- Navigation -->
    <nav class="navbar navbar-default custom_navbar_style">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/balu_logo.png" class="brand_style"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" id="dt-menu">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="index.php" id="home">Home</a>
                    </li>
                    <li class="page-scroll">
                        <a href="aboutus.php" id="aboutus">About Us</a>
                        <ul id="fof-sub-229" class="sub-nav">
                          <li id="menu-item-12581">
                            <span>Our History</span>
                          </li> 
                          <li id="menu-item-12455">
                            <a href="certifications.php">Certifications &amp; Awards</a>
                          </li>
                        </ul>
                    </li>
                    <li class="page-scroll">
                        <a href="manufacturing.php" id="manufacture">Manufacturing</a>
                    </li>
                    <li class="page-scroll">
                        <a href="approach.php" id="ourapproach">our approach</a>
                    </li>
                    <li class="page-scroll">
                        <a href="network.php" id="network">Network</a>
                    </li>
                    <li class="page-scroll">
                        <a href="sales_service.php" id="service">after sales service</a>
                    </li>
                    <!-- <li class="page-scroll">
                        <a href="sales_service.php">certifications</a>
                    </li> -->
                    <li class="page-scroll">
                        <a href="contactus.php" id="contactus">contact us</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Floating Bar of Header -->
    <div class="floating_wrapper">
      <div class="floating_header_content">
        <div class="col-md-2 col-sm-2 col-xs-2 floating-icon">
          <img src="img/phone.png" width="47" height="36">
        </div>
        <div class="col-md-9 col-sm-9 col-xs-9 floating-text" style="margin-left: 10px;">
          <span style="color:#5e000f; font-size:12px;">We're on call 24/7 </span><br>+91-22-26839916
        </div>
      </div>
      <div class="floating_header_content">
        <div class="col-md-2 col-sm-2 col-xs-2 floating-icon">
          <img src="img/clock.png" width="33" height="36">
        </div>
        <div class="col-md-9 col-sm-9 col-xs-9 floating-text">
          <span style="color:#5e000f;font-size:12px; ">Operating Hours </span><br> Mon - Fri 08:00 - 20:00
        </div>
      </div>
      <div class="floating_header_content">
        <div class="col-md-2 col-sm-2 col-xs-2 floating-icon">
          <img src="img/email.png" width="49" height="36">
        </div>
        <div class="col-md-9 col-sm-9 col-xs-9 floating-text" style="margin-left: 10px;">
          <span style="color:#5e000f;font-size:12px; ">Write To Us </span><br> <a class="mail_to" href="mailto:info@baluindustries.com">info@baluindustries.com</a>  
        </div>
      </div>
    </div>  


    <!-- Read More Modal Start -->
<div class="modal fade in" id="rm_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  
  <div class="modal-dialog" style="margin-top:7%; height:74%; overflow:hidden;">
    <div class="modal-content" style="height:100%;">
      <div class="modal-header" style="background-color: #df0024;border-bottom:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="">
            <span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title text-center" id="myModalLabel" style="color:#fff;font-family:helvetica; text-transform:uppercase;font-size:22px;">
            
        </h3>
      </div>
      <div class="modal-body clearfix text-center" id="modal_body">
        <img src="" style="display:none;">
        <div class="sub_txt" style="padding:0px 20px; text-align:justify;"> </div>
      </div>
    </div>
  </div>
</div>
<!-- Read More Modal End -->

<!-- Distributor Modal Start -->
<div class="modal fade in" id="donarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
  
  <div class="modal-dialog" style="margin-top:10%">
  <div class="modal-content">
    <div class="modal-header" style="background-color: #df0024;border-bottom:none;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=""><span aria-hidden="true">×</span></button>
    <h3 class="modal-title text-center" id="myModalLabel" style="color:#fff;font-family:helvetica; font-size:22px;">BECOME A DISTRIBUTOR</h3>
    </div>
    <div class="modal-body clearfix">
    <form class="form-group col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 custom_form custom_form_modal" name="contactForm3" id="contactForm3" method="POST" action="#">
        
    <div class="form-group">
      <input class="form-control custom-form-control form-name" type="text" name="name3" id="name3" placeholder="Name">
    </div>
    <div class="form-group">
      <input class="form-control custom-form-control form-email" type="text" name="email3" id="email3" placeholder="Email ID">
    </div>
    <div class="form-group">
      <input class="form-control custom-form-control form-phone" type="text" name="phone3" id="phone3" placeholder="Phone No.">
    </div>
    <div class="form-group">
      <input class="form-control custom-form-control" type="text" name="message3" id="message3" placeholder="Location">
    </div>
    <div class="form-group">
      <button type="button" class="btn btn-default submit_contact col-md-12 custom-form-control submit-btn-form" data="#contactform" id="modal_send">Submit</button>
    </div>
    </form>
    </div>
  </div>
  </div>
</div>
<!-- Distributor Modal End -->