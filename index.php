<!DOCTYPE html>

<!-- hello -->
<!-- hello -->
<!-- hello -->
<!-- hello -->
<!-- hello -->
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <!-- Slider  -->
    <!-- <div class="container-fluid padding-zero slider_start">
      <img src="img/balu_slider.jpg" class="width-100-percent">
    </div>
 -->
    
    <div class="container-fluid padding-zero">
        <div id="carousel-example-generic" class="carousel slide slider_start">
          <!-- Indicators -->
          <!-- <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol> -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <!-- First slide -->
            <div class="item active deepskyblue">

              <div class="carousel-caption">
                <h3 data-animation="animated fadeInLeft">
                 <img src="img/slider_effect.png">
                </h3>
                <h3 data-animation="animated fadeInLeft" class="slider_cap">
                  <span class="title_1_slider">You demand perfection,</span> <br>
                  <span class="title_1_slider">Balu delivers.</span>  <br>
                  <span class="title_3_slider">
                    In today’s market there is no room for error. That’s why, at Balu 
                    India, we’re continuously working to improve quality. It starts 
                    with having the latest technology and most talented employees.
                  </span>
                </h3>
              </div>
            </div> <!-- /.item -->
            <!-- First slide -->
            <div class="item deepskyblue">

              <div class="carousel-caption">
                <h3 data-animation="animated fadeInLeft">
                 <img src="img/slider_effect.png">
                </h3>
                <h3 data-animation="animated fadeInLeft" class="slider_cap">
                  <span class="title_1_slider">A name symbolizing </span> <br>
                  <span class="title_1_slider">quality and excellence.</span>  <br>
                  <span class="title_3_slider">
                    Balu’ is now an avant-garde manufacturer of fully finished and 
                    semi-finished forged crankshafts.
                  </span>
                </h3>
              </div>
            </div> <!-- /.item -->
            <!-- First slide -->
            <!-- <div class="item deepskyblue">

              <div class="carousel-caption">
                <h3 data-animation="animated fadeInLeft">
                 <img src="img/slider_effect.png">
                </h3>
                <h3 data-animation="animated fadeInLeft" class="slider_cap">
                  <span class="title_1_slider">You demand perfection.</span> <br>
                  <span class="title_1_slider">BALU DELIVERS.</span>  <br>
                  <span class="title_3_slider">
                    In today’s market, there’s no room for error. That’s why, at Balu India, 
                    we’re<br>continuously working to improve quality. It starts with having the 
                    latest<br>technology and the most talented employees.</span>
                </h3>
              </div>
            </div> --> <!-- /.item -->
          </div><!-- /.carousel-inner -->

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div><!-- /.carousel -->

    </div><!-- /.container -->
    <!-- Below Slider -->
    <!-- <div class="container-fluid padding-zero text-center below_slider_text">
        <p>Looking For a Quality and Affordable Heavy Equipment for Your Next Project?</p>
    </div> -->

    <div class="container-fluid bg_wrapper_core padding-zero">
      <div class="container whole_wrapper">
        <div class="col-md-12 core_wrap">
          <p class="title_1 with_line">CORE COMPETENCE IN CRANKSHAFTS</p>
          <div class="_line_"><hr></div>
          <p class="desc_1">‘Balu’ has become a name symbolising quality & excellence in the field of crankshaft manufacturing since its inception in 1990. Our in-house capability & state of the art automotive engineering enables us to manufacture any type of crankshaft in a large range of applications namely Automotive, Agricultural, Marine & Industrial. We have developed a very extensive range of forged crankshafts for leading Original Equipment Manufacturers within India and the rest of the world & a strong aftermarket presence in over 80 countries.The ISO/TS16949:2009 accreditation of our units in 2012 by TUV Nord Cert Gmbh added to our competitive edge making ‘Balu’ one of the very few companies to have this accreditation in the field of manufacturing crankshafts.</p>
          <p class="desc_1">
            Balu’ is now an avant-garde manufacturer of fully finished and semi-finished forged crankshafts and guarantee to manufacture as per customer requirements & market demands. Our incremental innovation & continuous strive to improve has awarded us by making us the only company to have the capability to manufacture crankshafts conforming to EURO III / Bharat IV norms.
          </p>
          <p class="desc_1">
            The manufacturing of the crankshafts is done with the latest equipments, instruments, technologies and highly skilled workforce which provide exceptional control over the entire process of manufacturing the crankshafts & strict adherence to Six Sigma & 8D disciplines.‘Balu’ is the supplier of choice of major OEM's not only in India but around the world due to our technological advantage and the highest standards of quality in the industry. All the crankshafts are manufactured to exact O.E specifications and on CNC lines, to ensure precision at every stage.</p>
        </div>
      </div>
    </div>
    <!-- Above Accolades -->
    <div class="homeSect3">
      <div class="container">
        <div class="homeSect3Inner">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4 skil" style="opacity: 0.4;">
                <div class="col-md-4">
                  <img id="awardimg" src="img/firstHover.png" style="text-align:center;">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1">certification</p>
                  <p>
                    ‘Balu’ is a ISO/TS 16949:2009 accredited company & we are in the 
                    process of shortly acquiring a ISO/TS 14001 certification for our 
                    continuous efforts in environmental conservation.
                  </p>
                  <button class="btn btn-danger" id="rm4" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; padding-bottom: 3px; border-radius:0px; display: none; background-color: rgb(255, 255, 255);">
                    READ MORE
                  </button>
                </div>

                <div class="readMoreInfo" id="rm41" style="display:none;">
                  <h3>certification</h3>
                  <div class="clearfix"></div>
                  <div class="rdImg"><img src="img/homeCerti.png"></div>
                </div>

              </div>

              <div class="col-md-4 skil" style="opacity: 0.4;">
                <div class="col-md-4" style="text-align:center">
                  <img id="crsimg" src="img/second.png">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1">ACCOLADES</p>
                  <p>
                    Balu’ has been rewarded for its excellence in automotive engineering 
                    by the Automotive Component Manufacturing Association (ACMA) which 
                    is a GOVT of India Company. 
                  </p>
                  <button class="btn btn-danger" id="rm5" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; border-radius:0px; padding-bottom: 3px; display: none; background-color: rgb(255, 255, 255);">
                    READ MORE</button>
                </div>

                <div class="readMoreInfo" id="rm51" style="display:none;">
                  <h3>ACCOLADES</h3>
                  <div class="clearfix"></div>
                  <div class="sub_txt">
                    
                      <h5 style="text-align:center;">ACMA Export Award 1999 – 2000</h5>
                      <img src="img/accoPic1.png">
                      <div class="clearfix"></div>
                      <h5 style="text-align:center;">ACMA Export Award 2000 – 2001</h5>
                      <img src="img/accoPic2.png">
                      <div class="clearfix"></div>
                      <h5 style="text-align:center;">ACMA Export Award 2002 – 2003</h5>
                      <img src="img/accoPic3.png">
                      <div class="clearfix"></div>
                      <h5 style="text-align:center;">ACMA Export Award 2003 – 2004 </h5>
                      <img src="img/accoPic4.png">
                      <div class="clearfix"></div>
                   
                  </div>
                </div>
              </div>

              <div class="col-md-4 skil" style="opacity: 0.4;" >
                <div class="col-md-4" style="text-align:center">
                  <img id="newsimg" src="img/third.png">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1" style="width:285px;">Guarantee policy</p>
                  <p style="width:285px;">
                    We guarantee that all the goods that have been manufactured by us are 
                    free from defects in material and workmanship.
                  </p>
                  
                    <button class="btn btn-danger" id="rm6" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; border-radius:0px; padding-bottom: 3px; display: none; background-color: rgb(255, 255, 255);">
                      READ MORE
                    </button>
                </div>

                <div class="readMoreInfo" id="rm61" style="display:none;">
                  <h3>Guarantee policy</h3>
                  <div class="clearfix"></div>
                  <div class="sub_txt">
                    We guarantee that all the goods that have been manufactured by us are 
                    free from defects in material and workmanship. We further guarantee 
                    that all the parts that are defective in material and workmanship are 
                    repairable and/or interchangeable. Any replaced parts will be our 
                    property. The obligation under this warranty does not include the 
                    refunding of these losses - Loss of profit, assembly expenses, customs 
                    charges, inland freight, all local charges etc.. We shall not be 
                    responsible for the wear out and / or the defects that arise in the 
                    transit and/or in the case that any alteration is carried out on the 
                    goods shipped by us, we will not be responsible for the defects 
                    weather it is due to the alteration or not. We shall also not be 
                    liable for the treatment of the shipped goods that is not in 
                    accordance with our regulations regarding its use. We further 
                    warranty that all our goods are interchangeable with the corresponding 
                    parts offered by the Original Equipment Manufacturer. All claims have 
                    to be submitted to us in writing within 6 months from the shipment of 
                    the goods and material may not be returned to us without our prior 
                    consent. In any case, they have to be returned to us free of charge 
                    and freight charges will only be returned if the claim is found 
                    justified after inspection of the goods by our quality control 
                    department.                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
    <!-- ACCOLADES -->
    <!-- <div class="container whole_certificate_wrapper text-center">
      <div class="col-md-12">
        <div class="col-md-12">
          <p class="title_1">CERTIFICATION</p>
        </div>
        <div class="col-md-6 certificate_wrap">
          <img src="img/certificate_1.jpg" />
        </div>
        <div class="col-md-6 certificate_wrap">
          <img src="img/certificate_2.jpg" />
        </div>
        <div class="col-md-12">
          <p class="title_1">CATALOGUE</p>
          <img src="img/click_down.jpg" />
        </div>
      </div>
    </div> -->

      <!-- Services -->
      <section id="balu_approach">
        <div class="container">
          <div class="row">
            <div class="col-md-12 core_wrap">
              <p class="title_1 with_line">BALU APPROACH</p>
              <div class="_line_"><hr></div>
              <p class="below_heading container">
                We have developed a very extensive range of forged crankshafts for leading 
                Original Equipment Manufacturers within India and the SAARC region.
              </p>
            </div>

            <div class="col-md-12">
              <div class="wrapper_app" id="homeBtmSlider">

          <div class="item">
            <div class="col-md-12 item_wrap">
              <p class="tit1">TECHNOLOGY</p>
              <p class="sub_txt">
                ‘BALU’, is now an avant-garde manufacturer of fully finished and 
                semi-finished forged crankshafts. The machining of the crankshafts 
                is done with the latest equipments, instruments, technologies and 
                highly skilled workforce which provide exceptional control over the 
                entire process of manufacturing the crankshafts. 
              </p>
              <!-- <div class="r_m_btn" id="rm1" onclick="openRMmodal(this.id);">READ MORE</div> -->
              <a href="approach.php"><div class="r_m_btn">READ MORE</div></a>
            </div>

            <div class="readMoreInfo" id="rm11" style="display:none;">
              <h3>TECHNOLOGY</h3>
              <div class="clearfix"></div>
              <p class="sub_txt">We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.
              We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.
              We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.
              We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.
              We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.
              We believe in the spirit of American manufacturing
                and work with our colleagues, partners, and
                competitors – along with local educational
                institutions – to keep good jobs here at home.</p>
            </div>
          </div>

          <div class="item">
            <div class="col-md-12 item_wrap">
              <p class="tit1">INNOVATION</p>
              <p class="sub_txt">
                ‘Balu’ has incremental innovation as a fore-front of the R&D with constant 
                commitment and interest in the application of the new technologies. 
                The company strives to be a leader in every aspect of its business.
              </p>
              <!-- <div id="rm2" onclick="openRMmodal(this.id);" class="r_m_btn">READ MORE</div> -->
              <a href="approach.php"><div class="r_m_btn">READ MORE</div></a>
            </div>

            <div class="readMoreInfo" id="rm21" style="display:none;">
              <h3>INNOVATION</h3>
              <div class="clearfix"></div>
              <p class="sub_txt">If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.
If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.
If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.
If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.
If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.
If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.</p>
            </div>
          </div>

          <div class="item">
            <div class="col-md-12 item_wrap">
              <p class="tit1">NEW PRODUCT DEVELOPMENT</p>
              <p class="sub_txt">
                The company has gained expertise in manufacturing of crankshafts for 
                more than 25 years in respect to process of material selection, heat 
                treatment, surface treatment etc.
              </p>
              <!-- <div id="rm3" onclick="openRMmodal(this.id);" class="r_m_btn">READ MORE</div> -->
              <a href="approach.php"><div class="r_m_btn">READ MORE</div></a>
            </div>

            <div class="readMoreInfo" id="rm31" style="display:none;">
              <h3>NEW PRODUCT DEVELOPMENT</h3>
              <div class="clearfix"></div>
              <p class="sub_txt">It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.</p>
            </div>
          </div>


          <div class="item">
            <div class="col-md-12 item_wrap">
              <p class="tit1">QUALITY AND EXCELLENCE </p>
              <p class="sub_txt">
                Quality, service and commitment is the primary objective of our company. 
                We always strive to meet the expectations, specifications and needs of 
                our customers. This is our commitment to maintain all operations in order 
                to guarantee complete stability in quality and service and thus full 
                customer satisfaction.
              </p>
              <!-- <div id="rm31" onclick="openRMmodal(this.id);" class="r_m_btn">READ MORE</div> -->
              <a href="approach.php"><div class="r_m_btn">READ MORE</div></a>
            </div>

            <div class="readMoreInfo" id="rm311" style="display:none;">
              <h3>NEW PRODUCT DEVELOPMENT</h3>
              <div class="clearfix"></div>
              <p class="sub_txt">It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.</p>
            </div>
          </div>
          

          <!-- <div class="item">
            <div class="col-md-12 item_wrap">
              <p class="tit1">NEW PRODUCT DEVELOPMENT</p>
              <p class="sub_txt">It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.</p>
              <div id="rm32" onclick="openRMmodal(this.id);" class="r_m_btn">READ MORE</div>
            </div>

            <div class="readMoreInfo" id="rm321" style="display:none;">
              <h3>NEW PRODUCT DEVELOPMENT</h3>
              <div class="clearfix"></div>
              <p class="sub_txt">It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.
It’s not a box to check or form to fill out. It’s how we
work. It’s how we live. We continuously push to
improve the effectiveness of our ISO 9001:2008 and
AS9100:2009 Rev C systems.</p>
            </div>
          </div> -->
           <!-- <div class="item col-md-4">
            <div class="col-md-12 item_wrap">
              <p class="tit1">INNOVATION</p>
              <p class="sub_txt">If you have specific processes, machines, or other
requirements, our machining team can help you
get the job done. We have expertise in pneumatics,
hydraulics, servo systems, and PLCs.</p>
              <a href="" class="r_m_btn">READ MORE</a>
            </div>
          </div> -->

        </div> <!-- endd of wrapp class -->
        </div>

        

      </section>

<?php include "footer.php"; ?>


  </body>
</html>
      
